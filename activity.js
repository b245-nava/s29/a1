// Users w/ letter s in first name or d in last name
db.users.find({
	$or: [
	{
		firstName: { $regex: "s", $options: "$i"}
	},
	{
		lastName: { $regex: "d", $options: "$i"}
	}]
},{
	_id: 0,
	firstName: 1,
	lastName: 1
});

// Users in HR dept and having age greater than or equal to 70
db.users.find({
	$and: [
	{
		department: "HR"
	},{
		age: {$gte: 70}
	}]
});

// Users with letter e in first name, age of less than or equal to 30
db.users.find({
	$and: [
		{
			firstName: {$regex: "e", $options: "$i"}
		},
		{
			age: {$lte: 30}
		}]
});